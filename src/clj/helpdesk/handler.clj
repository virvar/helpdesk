(ns helpdesk.handler
  (:require
    [compojure.core :refer [GET POST DELETE defroutes context]]
    [compojure.route :refer [resources]]
    [ring.util.response :refer [resource-response]]
    [ring.middleware.params :refer [wrap-params]]
    [ring.middleware.keyword-params :refer [wrap-keyword-params]]
    [ring.middleware.reload :refer [wrap-reload]]
    [ring.middleware.format :refer [wrap-restful-format]]
    [helpdesk.db :as db]
    [helpdesk.bids :as bids]
    ))

(defn- empty-view []
  (resource-response "index.html" {:root "public"}))

(defroutes
  api-routes
  (GET "/bids" [] (bids/get-bids))
  (POST "/bid" request (bids/create-bid request)))

(defroutes
  routes
  (context "/api" [] api-routes)
  (GET "/" [] (empty-view))
  (GET "/new-bid" [] (empty-view))
  (resources "/"))

(defonce app
         (-> #'routes
             wrap-keyword-params
             (wrap-restful-format :formats [:transit-json])
             wrap-params))

(def dev-handler (-> app wrap-reload))

(def handler app)

(defn init []
  (db/init))

(when-not *compile-files*
  (defonce start (init)))
