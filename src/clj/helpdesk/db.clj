(ns helpdesk.db
  (:require
    [clojure.java.io :as io]
    [ring.util.response :refer [response]]
    [datomic.api :as d]
    ))

(def db-url "datomic:mem://helpdesk")
(defonce conn (atom nil))

(defn- transact-resource [path]
  (->> path io/resource slurp read-string (d/transact @conn)))

(defn init []
  (d/create-database db-url)
  (reset! conn (d/connect db-url))
  @(transact-resource "schema.edn")
  @(transact-resource "sample-data.edn"))
