(ns helpdesk.bids
  (:require
    [ring.util.response :refer [response]]
    [datomic.api :as d]
    [helpdesk.db :as db]
    ))

(def bid-pattern
  [:bid/num
   :bid/title
   :bid/description
   :bid/author
   :bid/executor
   :bid/execution-date])

(defn get-bids []
  (->> (d/q '[:find [(pull ?id pattern) ...]
              :in $ pattern
              :where [?id :bid/num]]
            (d/db @db/conn)
            bid-pattern)
       (sort-by :bid/num)
       reverse
       response))

(defn- create-bid-db [{:bid/keys [title description author executor execution-date]}]
  @(d/transact @db/conn
               [{:db/id              (d/tempid :db.part/user -1)
                 :bid/title          title
                 :bid/description    description
                 :bid/author         author
                 :bid/executor       executor
                 :bid/execution-date execution-date}
                [:bid.fn/assign-num (d/tempid :db.part/user -1)]]))

(defn create-bid [request]
  (let [bid (:params request)]
    (create-bid-db bid)
    (response {})))
