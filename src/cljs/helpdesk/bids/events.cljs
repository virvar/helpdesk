(ns helpdesk.bids.events
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljs.core.async :refer [<! chan]]
    [cljs-http.client :as http]
    [re-frame.core :as re-frame :refer [reg-event-db]]
    ))

(reg-event-db
  ::set-bids
  (fn [db [_ bids]]
    (assoc db :bids bids)))

(defn- load-bids []
  (go (let [response (<! (http/get "/api/bids"))
            state (:body response)]
        (re-frame/dispatch [::set-bids state]))))

(reg-event-db
  ::load-bids
  (fn [db _]
    (load-bids)
    db))

(reg-event-db
  ::create-bid
  (fn [db [_ bid]]
    (go (let [response (<! (http/post (str "/api/bid")
                                      {:transit-params bid}))]
          (if (= (:status response) 200)
            (do (re-frame/dispatch [::load-bids])
                (re-frame/dispatch [:helpdesk.events/set-route :home-panel]))
            (js/alert "Ошибка при сохранении"))))
    db))
