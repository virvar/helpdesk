(ns helpdesk.bids.subs
  (:require
    [re-frame.core :as re-frame]
    ))

(re-frame/reg-sub
  ::bids
  (fn [db _]
    (:bids db)))
