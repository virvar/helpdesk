(ns helpdesk.bids.views.new-bid
  (:require
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    ))

(defn- always-int [s]
  (let [n (js/parseInt s 10)]
    (if (js/isNaN n) 0 n)))

(defn- create-bid [bid]
  (let [updated (update bid :bid/execution-date
                        (fn [{:keys [year month day]}]
                          (js/Date. (js/Date.UTC (always-int year)
                                                 (always-int (dec month))
                                                 (always-int day)))))]
    (re-frame/dispatch [:helpdesk.bids.events/create-bid updated])))

(defn- update-field-fn
  ([bid key]
   (update-field-fn bid key identity))
  ([bid key value-fn]
   (fn [e]
     (let [path (if (vector? key) key [key])
           target (.-target e)
           value (-> (if (= (.-type target) "checkbox")
                       (.-checked target)
                       (.-value target))
                     value-fn)]
       (swap! bid assoc-in path value)))))

(defn new-bid-panel []
  (let [bid (reagent/atom {})]
    (fn []
      [:div {}
       [:a {:class "btn btn-light mt-3"
            :href  "/"}
        "К списку заявок"]

       [:h1 "Создать заявку"]

       [:div {:class "form-group row"}
        [:label {:class "col-sm-2 col-form-label"
                 :for   "bid-title"}
         "Заголовок"]
        [:div {:class "col-sm-10"}
         [:input {:id        "bid-title"
                  :class     "form-control"
                  :type      "text"
                  :value     (:bid/title @bid)
                  :on-change (update-field-fn bid :bid/title)}]]]

       [:div {:class "form-group row"}
        [:label {:class "col-sm-2 col-form-label"
                 :for   "bid-description"}
         "Описание"]
        [:div {:class "col-sm-10"}
         [:input {:id        "bid-description"
                  :class     "form-control"
                  :type      "text"
                  :value     (:bid/description @bid)
                  :on-change (update-field-fn bid :bid/description)}]]]

       [:div {:class "form-group row"}
        [:label {:class "col-sm-2 col-form-label"
                 :for   "bid-author"}
         "Заявитель"]
        [:div {:class "col-sm-10"}
         [:input {:id        "bid-author"
                  :class     "form-control"
                  :type      "text"
                  :value     (:bid/author @bid)
                  :on-change (update-field-fn bid :bid/author)}]]]

       [:div {:class "form-group row"}
        [:label {:class "col-sm-2 col-form-label"
                 :for   "bid-executor"}
         "Исполнитель"]
        [:div {:class "col-sm-10"}
         [:input {:id        "bid-executor"
                  :class     "form-control"
                  :type      "text"
                  :value     (:bid/executor @bid)
                  :on-change (update-field-fn bid :bid/executor)}]]]

       [:div {:class "form-group row"}
        [:label {:class "col-sm-2 col-form-label"
                 :for   "bid-execution-date"}
         "Дата исполнения"]

        [:div {:class "col-sm-10 form-row"}
         [:div {:class "form-group col-sm-4 row"}
          [:label {:class "col-sm-3 col-form-label"
                   :for   "bid-execution-date"}
           "Год"]
          [:div {:class "col-sm-9"}
           [:input {:id        "bid-execution-date-year"
                    :class     "form-control"
                    :type      "text"
                    :value     (-> @bid :bid/execution-date :year)
                    :on-change (update-field-fn bid [:bid/execution-date :year])}]]]

         [:div {:class "form-group col-sm-4 row"}
          [:label {:class "col-sm-3 col-form-label"
                   :for   "bid-execution-date"}
           "Месяц"]
          [:div {:class "col-sm-9"}
           [:input {:id        "bid-execution-date-month"
                    :class     "form-control"
                    :type      "text"
                    :value     (-> @bid :bid/execution-date :month)
                    :on-change (update-field-fn bid [:bid/execution-date :month])}]]]

         [:div {:class "form-group col-sm-4 row"}
          [:label {:class "col-sm-3 col-form-label"
                   :for   "bid-execution-date"}
           "День"]
          [:div {:class "col-sm-9"}
           [:input {:id        "bid-execution-date-day"
                    :class     "form-control"
                    :type      "text"
                    :value     (-> @bid :bid/execution-date :day)
                    :on-change (update-field-fn bid [:bid/execution-date :day])}]]]]]

       [:button {:class    "btn btn-primary"
                 :type     "button"
                 :on-click #(create-bid @bid)}
        "Сохранить"]])))
