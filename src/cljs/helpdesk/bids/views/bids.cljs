(ns helpdesk.bids.views.bids
  (:require
    [clojure.string :as str]
    [re-frame.core :as re-frame]
    ))

(defn- date-str [date]
  (when date
    (first (str/split (.toISOString date) #"T"))))

(defn bids-panel []
  (let [bids (re-frame/subscribe [:helpdesk.bids.subs/bids])]
    (fn []
      [:div
       [:h1 "Заявки"]

       [:div {:class "table"}
        [:table {:class "table"}
         [:thead {}
          [:tr {}
           [:th "Номер"]
           [:th "Заголовок"]
           [:th "Описание"]
           [:th "Заявитель"]
           [:th "Исполнитель"]
           [:th "Дата исполнения"]]]
         [:tbody {}
          (doall
            (for [bid @bids]
              ^{:key (:bid/num bid)}
              [:tr {}
               [:td (:bid/num bid)]
               [:td (:bid/title bid)]
               [:td (:bid/description bid)]
               [:td (:bid/author bid)]
               [:td (:bid/executor bid)]
               [:td (date-str (:bid/execution-date bid))]]))]]]
       [:a {:class "btn btn-primary"
            :href  "/new-bid"}
        "Создать заявку"]])))
