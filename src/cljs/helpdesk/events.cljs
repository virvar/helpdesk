(ns helpdesk.events
  (:require
    [re-frame.core :refer [reg-event-db]]
    [helpdesk.db :as db]
    [helpdesk.routes :as routes]
    helpdesk.bids.events
    ))

(reg-event-db
  ::initialize-db
  (fn [_ _]
    db/default-db))

(reg-event-db
  ::set-active-panel
  (fn [db [_ active-panel]]
    (js/window.scrollTo 0 0)
    (assoc db :active-panel active-panel)))

(reg-event-db
  ::set-route
  (fn [db [_ active-panel]]
    (routes/set-route! active-panel)
    db))
