(ns helpdesk.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:require
    [secretary.core :as secretary]
    [accountant.core :as accountant]
    [re-frame.core :as re-frame]
    ))

(def page-url-name
  {:home-panel "/"})

(defn set-route! [page]
  (accountant/navigate! (get page-url-name page)))

(defn app-routes []
  (defroute "/" []
            (re-frame/dispatch [:helpdesk.events/set-active-panel :home-panel]))

  (defroute "/new-bid" []
            (re-frame/dispatch [:helpdesk.events/set-active-panel :new-bid-panel]))


  (accountant/configure-navigation!
    {:nav-handler
     (fn [path]
       (secretary/dispatch! path))
     :path-exists?
     (fn [path]
       (secretary/locate-route path))})
  (accountant/dispatch-current!))
