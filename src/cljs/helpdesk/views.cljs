(ns helpdesk.views
  (:require
    [re-frame.core :as re-frame]
    [helpdesk.bids.views.bids :as bids]
    [helpdesk.bids.views.new-bid :as new-bid]
    ))

(defn- panels [panel-name]
  (case panel-name
    :home-panel [bids/bids-panel]
    :new-bid-panel [new-bid/new-bid-panel]
    [:div]))

(defn show-panel [panel-name]
  [panels panel-name])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [:helpdesk.subs/active-panel])]
    (fn []
      [:div {:class "container"}
       [show-panel @active-panel]])))
