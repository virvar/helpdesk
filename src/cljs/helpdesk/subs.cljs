(ns helpdesk.subs
  (:require
    [re-frame.core :as re-frame]
    helpdesk.bids.subs
    ))

(re-frame/reg-sub
  ::active-panel
  (fn [db _]
    (:active-panel db)))
