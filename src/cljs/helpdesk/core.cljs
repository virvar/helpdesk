(ns helpdesk.core
  (:require
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    [helpdesk.routes :as routes]
    [helpdesk.views :as views]
    [helpdesk.config :as config]
    helpdesk.subs
    helpdesk.events
    ))

(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn- load-state []
  (re-frame/dispatch [:helpdesk.bids.events/load-bids]))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (load-state)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [:helpdesk.events/initialize-db])
  (dev-setup)
  (mount-root))
